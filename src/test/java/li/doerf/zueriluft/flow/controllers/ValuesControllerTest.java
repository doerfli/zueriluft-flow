package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.Config;
import li.doerf.zueriluft.flow.opendatazuerich.PollutantDataProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringJUnitConfig(Config.class)
@SpringBootTest
@AutoConfigureMockMvc
class ValuesControllerTest {
    @Autowired
    private PollutantDataProcessor pollutantDataProcessor;
    @Autowired
    private ValuesController controller;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() throws Exception {
        pollutantDataProcessor.process(this.getClass().getResourceAsStream("/sample_data.csv"));
    }

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void testLastByLocation1() throws Exception {
        this.mockMvc.perform(get("/values/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].value", is(113.55)))
                .andExpect(jsonPath("$.[0].instant", is("2018-07-25T16:00:00Z")))
                .andExpect(jsonPath("$.[0].pollutantId", is(5)))
                .andExpect(jsonPath("$.[0].pollutant").doesNotExist())
                .andExpect(jsonPath("$.[0].location").doesNotExist())
                .andExpect(jsonPath("$.[0].locationId").doesNotExist())
                .andExpect(jsonPath("$.[1].value", is(20.94)))
                .andExpect(jsonPath("$.[1].pollutantId", is(6)))
                .andExpect(jsonPath("$", hasSize(7)));
    }

    @Test
    void testLastByLocation2() throws Exception {
        this.mockMvc.perform(get("/values/2")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].value", is(101.05)))
                .andExpect(jsonPath("$.[0].instant", is("2018-07-25T16:00:00Z")))
                .andExpect(jsonPath("$.[0].pollutantId", is(12)))
                .andExpect(jsonPath("$.[0].pollutant").doesNotExist())
                .andExpect(jsonPath("$.[1].value", is(48.39)))
                .andExpect(jsonPath("$.[1].pollutantId", is(13)))
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    void testLastByLocation3() throws Exception {
        this.mockMvc.perform(get("/values/3")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    void testLastByLocation4() throws Exception {
        this.mockMvc.perform(get("/values/4")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void testLastByLocationSince1() throws Exception {
        this.mockMvc.perform(get("/values/1/2018-07-25T14:59:00Z")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].value", is(113.55)))
                .andExpect(jsonPath("$.[0].instant", is("2018-07-25T16:00:00Z")))
                .andExpect(jsonPath("$.[0].pollutantId", is(5)))
                .andExpect(jsonPath("$.[0].pollutant").doesNotExist())
                .andExpect(jsonPath("$.[0].location").doesNotExist())
                .andExpect(jsonPath("$.[0].locationId").doesNotExist())
                .andExpect(jsonPath("$.[1].value", is(112.86)))
                .andExpect(jsonPath("$.[1].pollutantId", is(5)))
                .andExpect(jsonPath("$.[1].instant", is("2018-07-25T15:00:00Z")))
                .andExpect(jsonPath("$.[2].value", is(20.94)))
                .andExpect(jsonPath("$.[2].pollutantId", is(6)))
                .andExpect(jsonPath("$.[2].instant", is("2018-07-25T16:00:00Z")))
                .andExpect(jsonPath("$", hasSize(14)));
    }

}
