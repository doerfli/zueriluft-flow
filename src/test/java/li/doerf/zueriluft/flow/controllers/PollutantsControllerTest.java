package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.Config;
import li.doerf.zueriluft.flow.opendatazuerich.PollutantDataProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringJUnitConfig(Config.class)
@SpringBootTest
@AutoConfigureMockMvc
class PollutantsControllerTest {
    @Autowired
    private PollutantDataProcessor pollutantDataProcessor;
    @Autowired
    private PollutantsController controller;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() throws Exception {
        pollutantDataProcessor.process(this.getClass().getResourceAsStream("/sample_data.csv"));
    }

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void testByLocation1() throws Exception {
        this.mockMvc.perform(get("/pollutants/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].name", is("Ozon")))
                .andExpect(jsonPath("$.[0].shortName", is("O₃")))
                .andExpect(jsonPath("$.[0].unit", is("µg/m³")))
                .andExpect(jsonPath("$.[0].location").doesNotExist())
                .andExpect(jsonPath("$.[0].locationId").doesNotExist())
                .andExpect(jsonPath("$.[1].name", is("Stickstoffdioxid")))
                .andExpect(jsonPath("$.[2].name", is("Feinstaub")))
                .andExpect(jsonPath("$.[3].name", is("Lufttemperatur")))
                .andExpect(jsonPath("$", hasSize(7)));
    }

    @Test
    void testByLocation2() throws Exception {
        this.mockMvc.perform(get("/pollutants/2")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].name", is("Ozon")))
                .andExpect(jsonPath("$.[0].shortName", is("O₃")))
                .andExpect(jsonPath("$.[0].unit", is("µg/m³")))
                .andExpect(jsonPath("$.[1].name", is("Stickstoffdioxid")))
                .andExpect(jsonPath("$.[2].name", is("Feinstaub")))
                .andExpect(jsonPath("$", hasSize(3)));
    }
}
