package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.Config;
import li.doerf.zueriluft.flow.opendatazuerich.MetadataProcessor;
import li.doerf.zueriluft.flow.opendatazuerich.PollutantDataProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;

import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringJUnitConfig(Config.class)
@SpringBootTest
@AutoConfigureMockMvc
class LocationsControllerTest {
    @Autowired
    private PollutantDataProcessor pollutantDataProcessor;
    @Autowired
    private MetadataProcessor metadataProcessor;
    @Autowired
    private LocationsController controller;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() throws Exception {
        pollutantDataProcessor.process(this.getClass().getResourceAsStream("/sample_data.csv"));
        metadataProcessor.process(new InputStreamReader(this.getClass().getResourceAsStream("/metadata.json")));
    }

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        this.mockMvc.perform(get("/locations")).andDo(print()).andExpect(status().isOk())
//                .andExpect(content().string(containsString("Stampfenbachstrasse")));
                .andExpect(jsonPath("$.[0].name", is("Stampfenbachstrasse")))
                .andExpect(jsonPath("$.[0].latitude", is(47.38676)))
                .andExpect(jsonPath("$.[0].longitude", is(8.5398)))
                .andExpect(jsonPath("$", hasSize(4)));
    }
}
