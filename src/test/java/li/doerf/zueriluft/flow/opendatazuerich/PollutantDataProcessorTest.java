package li.doerf.zueriluft.flow.opendatazuerich;

import li.doerf.zueriluft.flow.Config;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import li.doerf.zueriluft.flow.repositories.PollutantRepository;
import li.doerf.zueriluft.flow.repositories.ValueRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@SpringJUnitConfig(Config.class)
class PollutantDataProcessorTest {

    @Autowired
    private PollutantDataProcessor pollutantDataProcessor;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private PollutantRepository substanceRepository;
    @Autowired
    private ValueRepository valueRepository;

    @Test
    void testProcess() throws Exception {
        pollutantDataProcessor.process(this.getClass().getResourceAsStream("/sample_data.csv"));

        assertThat(locationRepository.count(), is(4L));
        assertThat(substanceRepository.count(), is(15L));
        assertThat(valueRepository.count(), greaterThan(15*5L));
    }

}
