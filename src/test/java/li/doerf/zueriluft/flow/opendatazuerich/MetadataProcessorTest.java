package li.doerf.zueriluft.flow.opendatazuerich;

import li.doerf.zueriluft.flow.Config;
import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.io.InputStreamReader;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringJUnitConfig(Config.class)
class MetadataProcessorTest {

    @Autowired
    private MetadataProcessor metadataProcessor;
    @Autowired
    private LocationRepository locationRepository;

    @BeforeEach
    void setup() {
        var loc1 = new Location();
        loc1.setName("Stampfenbachstrasse");
        locationRepository.save(loc1);

        var loc3 = new Location();
        loc3.setName("Rosengartenstrasse");
        locationRepository.save(loc3);
    }

    @Test
    @Ignore
    void testProcess() throws Exception {
        metadataProcessor.process(new InputStreamReader(this.getClass().getResourceAsStream("/metadata.json")));

        List<Location> result = locationRepository.findByName("Rosengartenstrasse");
        assertThat(result.size()).isEqualTo(1);
        var rogast = result.get(0);
        assertThat(rogast.getLatitude()).isEqualTo(47.39516);
        assertThat(rogast.getLongitude()).isEqualTo(8.52606);
    }
}
