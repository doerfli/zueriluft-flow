package li.doerf.zueriluft.flow;

import li.doerf.zueriluft.flow.opendatazuerich.PollutantDataProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableScheduling
public class LocalApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(LocalApplication.class, args);

        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                context.getBean(PollutantDataProcessor.class).process(LocalApplication.class.getResourceAsStream("/sample_data.csv"));
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        });
    }

}
