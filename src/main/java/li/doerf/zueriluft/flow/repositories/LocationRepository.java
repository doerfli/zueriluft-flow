package li.doerf.zueriluft.flow.repositories;

import li.doerf.zueriluft.flow.entities.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    List<Location> findByName(String name);
}
