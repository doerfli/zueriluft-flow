package li.doerf.zueriluft.flow.repositories;

import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.entities.Pollutant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PollutantRepository extends CrudRepository<Pollutant, Long> {
    List<Pollutant> findByNameAndLocation(String name, Location location);
    List<Pollutant> findAllByLocation(Location aLocation);
}
