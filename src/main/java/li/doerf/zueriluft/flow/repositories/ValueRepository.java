package li.doerf.zueriluft.flow.repositories;

import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.entities.Pollutant;
import li.doerf.zueriluft.flow.entities.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface ValueRepository extends CrudRepository<Value, Long> {
    List<Value> findFirstByPollutantAndPollutantLocationOrderByIdDesc(Pollutant pollutant, Location location);
    List<Value> findByPollutantAndPollutantLocationAndInstantAfterOrderByIdDesc(Pollutant pollutant, Location location, Instant after);


}
