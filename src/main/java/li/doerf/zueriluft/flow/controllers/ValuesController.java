package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.dtos.ValueDto;
import li.doerf.zueriluft.flow.entities.Value;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import li.doerf.zueriluft.flow.repositories.PollutantRepository;
import li.doerf.zueriluft.flow.repositories.ValueRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ValuesController {
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final LocationRepository locationRepository;
    private final PollutantRepository pollutantRepository;
    private final ValueRepository valueRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ValuesController(LocationRepository locationRepository, PollutantRepository pollutantRepository, ValueRepository valueRepository, ModelMapper modelMapper) {
        this.locationRepository = locationRepository;
        this.pollutantRepository = pollutantRepository;
        this.valueRepository = valueRepository;
        this.modelMapper = modelMapper;
    }

    @RequestMapping("/values/{locationId}")
    public List<ValueDto> lastByLocation(@PathVariable Long locationId) {
        logger.debug("last values by location");
        var location = locationRepository.findById(locationId).orElse(null);

        if (location == null) {
            throw new IllegalArgumentException("location " + locationId + " not found");
        }

        var pollutants = pollutantRepository.findAllByLocation(location);

        // get list of value by looping over each pollutant at this location
        var valueList = pollutants.stream().map(pollutant -> {
            logger.debug("last values for " + pollutant.getName());
            return valueRepository.findFirstByPollutantAndPollutantLocationOrderByIdDesc(pollutant, location);
        }).flatMap(List::stream);

        // convert to dto
        return valueList.map(this::convertToDto).collect(Collectors.toList());
    }

    @RequestMapping("/values/{locationId}/{aSince}")
    public List<ValueDto> lastByLocationSince(@PathVariable Long locationId, @PathVariable String aSince) {
        var since = Instant.parse(aSince);
        logger.debug("last values by location since " + since);

        var location = locationRepository.findById(locationId).orElse(null);

        if (location == null) {
            throw new IllegalArgumentException("location " + locationId + " not found");
        }

        var pollutants = pollutantRepository.findAllByLocation(location);

        // get list of value by looping over each pollutant at this location
        var valueList = pollutants.stream().map(pollutant -> {
            logger.debug("last values for " + pollutant.getName());
            return valueRepository.findByPollutantAndPollutantLocationAndInstantAfterOrderByIdDesc(
                    pollutant, location, since);
        }).flatMap(List::stream);

        // convert to dto
        return valueList.map(this::convertToDto).collect(Collectors.toList());
    }

    private ValueDto convertToDto(Value value) {
        return modelMapper.map(value, ValueDto.class);
    }

}
