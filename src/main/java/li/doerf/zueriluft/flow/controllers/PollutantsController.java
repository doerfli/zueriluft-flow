package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.dtos.PollutantDto;
import li.doerf.zueriluft.flow.entities.Pollutant;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import li.doerf.zueriluft.flow.repositories.PollutantRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PollutantsController {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final LocationRepository locationRepository;
    private final PollutantRepository pollutantRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public PollutantsController(LocationRepository locationRepository, PollutantRepository pollutantRepository, ModelMapper modelMapper) {
        this.locationRepository = locationRepository;
        this.pollutantRepository = pollutantRepository;
        this.modelMapper = modelMapper;
    }

    @RequestMapping("/pollutants/{locationId}")
    public List<PollutantDto> byLocation(@PathVariable Long locationId) {
        logger.debug("retrieving by locations");
        var location = locationRepository.findById(locationId).get();
        return pollutantRepository.findAllByLocation(location)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private PollutantDto convertToDto(Pollutant poll) {
        return modelMapper.map(poll, PollutantDto.class);
    }
}
