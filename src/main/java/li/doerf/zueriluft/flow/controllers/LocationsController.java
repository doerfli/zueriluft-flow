package li.doerf.zueriluft.flow.controllers;

import li.doerf.zueriluft.flow.dtos.LocationDto;
import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class LocationsController {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final LocationRepository locationRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public LocationsController(LocationRepository locationRepository, ModelMapper modelMapper) {
        this.locationRepository = locationRepository;
        this.modelMapper = modelMapper;
    }

    @RequestMapping("/locations")
    public List<LocationDto> getAll() {
        logger.debug("get all");
        var list = new ArrayList<Location>();
        locationRepository.findAll().iterator().forEachRemaining(list::add);
        return list.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private LocationDto convertToDto(Location location) {
        return modelMapper.map(location, LocationDto.class);
    }


}
