package li.doerf.zueriluft.flow.dtos;

public class PollutantDto {
    private Long id;
    private String name;
    private String shortName;
    private String unit;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public String getName() {
        return name;
    }

    public void setName(String aName) {
        name = aName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String aShortName) {
        shortName = aShortName;
    }

}
