package li.doerf.zueriluft.flow.dtos;

import java.math.BigDecimal;
import java.time.Instant;

public class ValueDto {

    private Long id;
    private BigDecimal value;
    private Instant instant;
    private Long pollutantId;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal aValue) {
        value = aValue;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant aInstant) {
        instant = aInstant;
    }

    public Long getPollutantId() {
        return pollutantId;
    }

    public void setPollutantId(Long pollutantId) {
        this.pollutantId = pollutantId;
    }
}
