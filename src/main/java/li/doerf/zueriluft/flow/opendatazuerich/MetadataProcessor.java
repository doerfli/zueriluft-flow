package li.doerf.zueriluft.flow.opendatazuerich;

import com.google.gson.Gson;
import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MetadataProcessor {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final LocationRepository locationRepository;

    public MetadataProcessor(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public void process(Reader byteStream) throws IOException {
        String jsonString = convert(byteStream);
        logger.debug(jsonString);

        Gson gson = new Gson();

        ZueriluftMetadata metadata = gson.fromJson(jsonString, ZueriluftMetadata.class);

        for (Map.Entry<String,ZLStation> e : metadata.stationen.entrySet()) {
            var station = e.getValue();
            List<Location> locations = locationRepository.findByName(station.Kurzname);
            if (locations.isEmpty()) {
                continue;
            }
            Location location = locations.get(0);
            if (location.getLatitude() != null && location.getLongitude() != null) {
                continue;
            }
            location.setLatitude(station.Koordinaten.get("wgs84").get("lat"));
            location.setLongitude(station.Koordinaten.get("wgs84").get("lng"));
            locationRepository.save(location);
            logger.info("updated location with latitude and longitude");
        }
    }

    private String convert(Reader reader) throws IOException {
        try (BufferedReader br = new BufferedReader(reader)) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }


    static class ZueriluftMetadata {
        HashMap<String,ZLStation> stationen;
    }

    static class ZLStation {
        String ID;
        String Name;
        String Kurzname;
        HashMap<String,HashMap<String,Double>> Koordinaten;
    }

}
