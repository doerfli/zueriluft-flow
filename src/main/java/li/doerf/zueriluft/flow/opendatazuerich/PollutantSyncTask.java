package li.doerf.zueriluft.flow.opendatazuerich;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;

@Component
public class PollutantSyncTask {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final PollutantDataProcessor pollutantDataProcessor;
    private final MetadataProcessor metadataProcessor;

    public PollutantSyncTask(PollutantDataProcessor processor, MetadataProcessor metadataProcessor) {
        this.pollutantDataProcessor = processor;
        this.metadataProcessor = metadataProcessor;
    }

    @Scheduled(cron = "30 57 * * * *")
    public synchronized void execute() {
        downloadPollutant();
        downloadMetadata();
    }

    private synchronized void downloadPollutant() {
        Request request = new Request.Builder()
                .url("http://ogd.zueriluft.ch/api/v1/h1.csv")
                .build();
        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            pollutantDataProcessor.process(response.body().byteStream());
        } catch (IOException | ParseException aE) {
            logger.error("caught exception while processing pollutant data", aE);
        }
    }

    private void downloadMetadata() {
        Request request = new Request.Builder()
                .url("http://ogd.zueriluft.ch/api/v1/metadaten.json")
                .build();
        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            metadataProcessor.process(response.body().charStream());
        } catch (IOException aE) {
            logger.error("caught exception while processing metadata", aE);
        }
    }

}
