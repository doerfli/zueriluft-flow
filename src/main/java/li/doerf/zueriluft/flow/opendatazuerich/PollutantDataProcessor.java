package li.doerf.zueriluft.flow.opendatazuerich;

import com.opencsv.CSVReader;
import li.doerf.zueriluft.flow.entities.Location;
import li.doerf.zueriluft.flow.entities.Pollutant;
import li.doerf.zueriluft.flow.entities.Value;
import li.doerf.zueriluft.flow.repositories.LocationRepository;
import li.doerf.zueriluft.flow.repositories.PollutantRepository;
import li.doerf.zueriluft.flow.repositories.ValueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PollutantDataProcessor {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final LocationRepository locationRepository;
    private final PollutantRepository substanceRepository;
    private final ValueRepository valueRepository;

    public PollutantDataProcessor(LocationRepository locationRepository, PollutantRepository substanceRepository, ValueRepository valueRepository) {
        this.locationRepository = locationRepository;
        this.substanceRepository = substanceRepository;
        this.valueRepository = valueRepository;
    }


    public void process(InputStream aStream) throws IOException, ParseException {
        logger.info("processing downloaded data start");
        try (CSVReader reader = new CSVReader(new InputStreamReader(aStream, "ISO8859_1"), ';')) {
            List<String> locationHeader = Arrays.asList(reader.readNext());
            reader.skip(1); // ignore second location header
            List<String> substanceHeader = Arrays.asList(reader.readNext());
            List<String> substanceShortHeader = Arrays.asList(reader.readNext());
            reader.skip(1); // ignore next line (??)
            List<String> substanceUnitHeader = Arrays.asList(reader.readNext());

            storeLocations(locationHeader);
            var unitColumMap = storePollutants(locationHeader, substanceHeader, substanceShortHeader, substanceUnitHeader);

            valueRepository.deleteAll();
            String[] row;
            while((row = reader.readNext()) != null ) {
                processRow(unitColumMap, row);
            }
        }
        logger.info("processing downloaded data finished");
    }

    private void storeLocations(List<String> aLocations) {
        var locations = new ArrayList<>(aLocations);
        locations.remove(0); // skip first column
        locations.forEach(loc -> {
            var location = resolveLocationName(loc);
            if ( locationRepository.findByName(location).size() > 0) {
                logger.debug("location exists: " + location);
                return;
            }
            saveNewLocation(location);
        });
    }

    private void saveNewLocation(String location) {
        Location newLoc = new Location();
        newLoc.setName(location);
        locationRepository.save(newLoc);
        logger.debug("location saved: " + location);
    }

    private Map<Integer, Pollutant> storePollutants(
            List<String> locationList, List<String> substanceList, List<String> shortList, List<String> unitList) {
        var locations = new ArrayList<>(locationList);
        var substances = new ArrayList<>(substanceList);
        var substancesShort = new ArrayList<>(shortList);
        var substancesUnit = new ArrayList<>(unitList);
        // skip all first column
        locations.remove(0);
        substances.remove(0);
        substancesShort.remove(0);
        substancesUnit.remove(0);

        var result = new HashMap<Integer, Pollutant>();

        for(int i = 0; i < locations.size(); i++) {
            String locationS = resolveLocationName(locations.get(i));
            String substanceS = resolveSubstanceName(substances.get(i));
            String shortS = resolveShortName(substancesShort.get(i));
            String unitS = resolveUnit(substancesUnit.get(i));

            var location = locationRepository.findByName(locationS).get(0);
            var substanceL = substanceRepository.findByNameAndLocation(substanceS, location);
            var substance = substanceL.size() > 0 ? substanceL.get(0) : null;
            if (substance == null ) {
                substance = saveNewPollutant(substanceS, shortS, unitS, location);
            }

            result.put(i, substance);
        }

        return result;
    }

    private Pollutant saveNewPollutant(String aSubstanceS, String aShortS, String aUnitS, Location aLocation) {
        Pollutant substance;
        substance = new Pollutant();
        substance.setName(aSubstanceS);
        substance.setShortName(aShortS);
        substance.setUnit(aUnitS);
        substance.setLocation(aLocation);
        substanceRepository.save(substance);
        logger.debug("substance saved: " + aLocation);
        return substance;
    }

    private void processRow(Map<Integer, Pollutant> aUnitColumnMap, String[] aRow) throws ParseException {
        var cols = new ArrayList<>(Arrays.asList(aRow)); // modifiable list
        var dateS = cols.remove(0).trim();
        var date = new SimpleDateFormat("dd.MM.yyyy HH:mm z").parse(String.format("%s UTC+01:00", dateS));

        for(int i = 0; i < cols.size(); i++) {
            String v = cols.get(i).trim();
            if ( v.equals("NaN")) {
                continue; // skip not existent data point
            }
            var valueB = BigDecimal.valueOf(Double.parseDouble(v));
            saveNewValue(aUnitColumnMap, date, i, valueB);
        }
    }

    private void saveNewValue(Map<Integer, Pollutant> aUnitColumnMap, Date aDate, int aI, BigDecimal aValueB) {
        var value = new Value();
        value.setValue(aValueB);
        value.setInstant(aDate.toInstant());
        value.setPollutant(aUnitColumnMap.get(aI));
        valueRepository.save(value);
        logger.debug("value saved: " + value);
    }

    private String resolveLocationName(String loc) {
        var l = loc.trim();
        if(l.startsWith("Zürich")) {
            l = l.substring(7);
        }
        return l;
    }

    private String resolveSubstanceName(String substance) {
        var sub = substance.trim();
        if(sub.endsWith(" PM10")) {
            sub = sub.substring(0, sub.indexOf(" PM10"));
        }
        return sub;
    }

    private String resolveShortName(String shor) {
        var s = shor.trim();
        s = s.replace("O2", "O\u2082");
        s = s.replace("O3", "O\u2083");
        return s;
    }

    private String resolveUnit(String s) {
        var u = s.trim();
        u = u.replace("m3", "m\u00B3");
        return u;
    }

}
