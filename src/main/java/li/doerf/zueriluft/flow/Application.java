package li.doerf.zueriluft.flow;

import li.doerf.zueriluft.flow.opendatazuerich.PollutantSyncTask;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executors;

@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        Executors.newSingleThreadExecutor().execute(() ->
            context.getBean(PollutantSyncTask.class).execute()
        );
    }

}
