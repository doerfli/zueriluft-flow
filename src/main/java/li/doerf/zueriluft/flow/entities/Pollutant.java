package li.doerf.zueriluft.flow.entities;

import javax.persistence.*;

@Entity
public class Pollutant {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String shortName;
    private String unit;
    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public String getName() {
        return name;
    }

    public void setName(String aName) {
        name = aName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location aLocation) {
        location = aLocation;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String aShortName) {
        shortName = aShortName;
    }
}
