package li.doerf.zueriluft.flow.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
public class Value {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private BigDecimal value;
    private Instant instant;
    @ManyToOne
    @JoinColumn(name = "pollutant_id")
    private Pollutant pollutant;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal aValue) {
        value = aValue;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant aInstant) {
        instant = aInstant;
    }

    public Pollutant getPollutant() {
        return pollutant;
    }

    public void setPollutant(Pollutant aPollutant) {
        pollutant = aPollutant;
    }
}
