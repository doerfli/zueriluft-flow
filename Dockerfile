#= Build ============================================================
FROM adoptopenjdk/openjdk15-openj9:alpine-slim as build
WORKDIR /workspace

COPY build.gradle .
COPY gradlew .
COPY settings.gradle .
COPY gradle gradle
COPY src src

RUN ./gradlew -Dorg.gradle.daemon=false bootJar
RUN mkdir -p build/libs/dependency && (cd build/libs/dependency; jar -xf ../*.jar)

#= Run ==============================================================
FROM adoptopenjdk/openjdk15-openj9:alpine-slim

EXPOSE 8080/tcp
VOLUME /tmp
VOLUME /log

COPY --from=build /workspace/build/libs/dependency/BOOT-INF/lib /app/lib
COPY --from=build /workspace/build/libs/dependency/META-INF /app/META-INF
COPY --from=build /workspace/build/libs/dependency/BOOT-INF/classes /app

ENTRYPOINT java -cp app:app/lib/* -Xtune:virtualized li.doerf.zueriluft.flow.Application
